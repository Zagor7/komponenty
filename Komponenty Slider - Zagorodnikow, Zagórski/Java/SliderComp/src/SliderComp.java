import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class SliderComp extends JPanel implements PropertyChangeListener{
    private int minValue;
    private int maxValue;
    private Label minLabel;
    private Label maxLabel;
    private Rectangle pointer;
    private Dimension dim;
    private int preX, preY;
    private double wartosc;
    SliderComp()
    {

        dim = new Dimension(10, 20);
        setPreferredSize(new Dimension(300, 45));
        this.setLayout(null);
        minLabel = new Label(String.valueOf(getMinValue()));
        maxLabel = new Label(String.valueOf(getMaxValue()));
        minLabel.setBounds(10, 35, 25, 10);
        maxLabel.setBounds(265, 35, 25, 10);
        this.add(minLabel);
        this.add(maxLabel);
        addMouseMotionListener(new MyMouseAdapter());
        addMouseListener(new MyMouseAdapter());
        pointer = new Rectangle(dim);
        pointer.setLocation(150, 10);
    }


    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g.setColor(Color.CYAN);
        g.fillRect(20, 10, 270, 20);
        g2d.setColor(Color.BLUE);
        g2d.fill(pointer);
        g2d.dispose();
    }




    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
        minLabel.setText(String.valueOf(minValue));
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        maxLabel.setText(String.valueOf(maxValue));
    }


    public Rectangle getPointer() {
        return pointer;
    }

    public void setPointer(Rectangle pointer) {
        this.pointer = pointer;
    }

    public int getPreY() {
        return preY;
    }

    public void setPreY(int preY) {
        this.preY = preY;
    }

    public int getPreX() {
        return preX;
    }

    public void setPreX(int preX) {
        this.preX = preX;
    }


    public double getWartosc() {
        return wartosc;
    }

    public void setWartosc(double wartosc) {
        wartosc = Math.round(wartosc*100)/100.0;
        this.wartosc = wartosc;



    }



    @Override
    public void propertyChange(PropertyChangeEvent evt) {

    }


    private class MyMouseAdapter extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            setPreX(e.getX());
            setPreY(e.getY());
            if((e.getX()>=20 && e.getX()< 281))
            {
                updateLocation(e);
            }
            firePropertyChange("wartosc", getPreX(), getWartosc());

        }

        @Override
        public void mouseDragged(MouseEvent e) {
                if(e.getX()>=20 && e.getX()< 281)
                {
                    updateLocation(e);
                }

            firePropertyChange("wartosc", getPreX(), getWartosc());
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if(pointer.contains(e.getX(), e.getY())&& (e.getX()>=20 && e.getX()< 281))
            {
                updateLocation(e);
            }
            else
            {
            }
            firePropertyChange("wartosc", getPreX(), getWartosc());

        }

        public void updateLocation(MouseEvent e) {
            pointer.setLocation(e.getX(), 10);
            setWartosc(((((pointer.getX()-20.0)/260.0))*(getMaxValue()-getMinValue()))+getMinValue());
            repaint();
        }
    }

}
