import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class CompTest {

    public static void main(String[] args) {
        SliderComp sc = new SliderComp();
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        frame.setLayout(new FlowLayout());
        frame.setSize(325, 250);
        sc.setMinValue(15);
        sc.setMaxValue(300);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

        JLabel lab1 = new JLabel("Wynik:");
        JLabel lab2 = new JLabel("0");
        lab1.setBounds(120, 100, 60, 15);
        lab2.setBounds(170, 100, 60, 15);
        panel.add(lab1);
        panel.add(lab2);
        frame.add(panel);

        frame.add(sc);
        sc.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                lab2.setText(String.valueOf(sc.getWartosc()));
            }
        });


    }
}
