﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.CompilerServices;

namespace nowySlider
{
    public partial class UserControl1 : UserControl
    {
        public event EventHandler ValueChanged;

        private float max, min, currentValue;
        private bool MouseIsDown = false, jumpB = false;
        private Color col;
        private Brush bruh;
        int jumpV;

        protected virtual void OnValueChanged()
        {
            if (ValueChanged != null) ValueChanged(this, EventArgs.Empty);
        }


        public UserControl1()
        {
            col = Color.Silver;
            bruh = Brushes.Silver;
            InitializeComponent();

        }

        private void ValidateMinMax()
        {
            if (min > max)
            {
                float temp = min;
                min = max;
                max = temp;
            }
            else if (min == max)
            {
                Console.WriteLine("Minimum and maximum are equal");
            }
            minLabel.Text = min.ToString();
            maxLabel.Text = max.ToString();
        }


        [Description("Set/Get the minimal value of the slider")]
        [Category("SliderSettings")]
        public float MinValue
        {
            get
            {
                return min;
            }

            set
            {
                min = value;
                ValidateMinMax();
            }
        }

        public void setJump(bool b, int v)
        {
            jumpB = b;
            jumpV = v;
        }


        [Description("Set/Get the maximal value of the slider")]
        [Category("SliderSettings")]
        public float MaxValue
        {
            get
            {
                return max;
            }
            set
            {

                max = value;
                ValidateMinMax();

            }
        }

        [Description("Set/Get the current value of the slider")]
        [Category("SliderSettings")]
        public float Value
        {
            set
            {
                if (jumpB == false)
                {
                    // Make sure the new value is within bounds.
                    if (value < min) value = min;
                    if (value > max) value = max;

                    // See if the value has changed.
                    if (currentValue == value) return;

                    // Save the new value.
                    currentValue = value;

                    // Redraw to show the new value.
                    pictureBox1.Refresh();

                    // Display the value tooltip.
                    int tip_x = pictureBox1.Left + (int)ValueToX(Value);
                    int tip_y = pictureBox1.Top;
                    tipValue.Show(currentValue.ToString("0.00"), this, tip_x, tip_y, 3000);
                    OnValueChanged();
                }
                else
                {
                    // Make sure the new value is within bounds.
                    if (value < min) value = min;
                    if (value > max) value = max;

                    if (value == min || value == max) {}
                    else {
                        float aa = value / jumpV;
                        currentValue = (float)jumpV * (float)Math.Round(aa);
                        if (currentValue < min) currentValue = min;
                        if (currentValue > max) currentValue = max;

                        // Redraw to show the new value.
                        pictureBox1.Refresh();

                        // Display the value tooltip.
                        int tip_x = pictureBox1.Left + (int)ValueToX(Value);
                        int tip_y = pictureBox1.Top;
                        tipValue.Show(Value.ToString("0.00"), this, tip_x, tip_y, 3000);
                        OnValueChanged();
                    }
                }
            }
            get
            {
                return currentValue;
            }
        }



        private float XtoValue(int x)
        {
            return min + (max - min) * x / (float)(pictureBox1.ClientSize.Width - 1);
        }

        // Convert value to an X coordinate.
        private float ValueToX(float value)
        {
            return (pictureBox1.ClientSize.Width - 1) *
                (value - min) / (float)(max - min);
        }

        // Draw the needle.
        private void picSlider_Paint(object sender, PaintEventArgs e)
        {
            // Calculate the needle's X coordinate.
            float x = ValueToX(currentValue);
            int y = (int)(pictureBox1.ClientSize.Height * 0.25);
            int hgt = pictureBox1.ClientSize.Height - 2 * y;

            // Draw it.
            e.Graphics.FillRectangle(bruh, 0, y, x, hgt);
            using (Pen pen = new Pen(col, 3))
            {
                if (pictureBox1.ClientSize.Height > 0 && pictureBox1.ClientSize.Width > 0 && x > 0)
                {
                    e.Graphics.DrawLine(pen, x, 0, x, pictureBox1.ClientSize.Height);
                }
            }
        }

        private void picSlider_MouseDown(object sender, MouseEventArgs e)
        {
            MouseIsDown = true;
            Value = (XtoValue(e.X));
        }
        private void picSlider_MouseMove(object sender, MouseEventArgs e)
        {
            if (!MouseIsDown) return;
            Value = (XtoValue(e.X));
        }
        private void picSlider_MouseUp(object sender, MouseEventArgs e)
        {
            MouseIsDown = false;
            tipValue.Hide(this);

        }

        [Description("Set back color of the slider")]
        [Category("Drawing")]
        public void ChangeColor(Color c)
        {
            pictureBox1.BackColor = c;
        }

        [Description("Set color of the slider and color of the trace")]
        [Category("Drawing")]
        public void ChangeSlider(Color pointer, Brush trace)
        {
            col = pointer;
            bruh = trace;
        }
    }
}