﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsControlLibrary1
{
    public partial class test: UserControl
    {
        public test()
        {
            InitializeComponent();
            userControl11.ChangeColor(Color.FromArgb(128,128,128));
            userControl11.setJump(true, 3);
            userControl11.MinValue = 99;
            userControl11.MaxValue = 1;
            userControl11.ValueChanged += updateLabel;
            
        }

        private void userControl11_Load(object sender, EventArgs e)
        {

        }

        public void updateLabel(Object sender, EventArgs e) { label1.Text = userControl11.Value.ToString(); }
    }
}
